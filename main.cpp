/**
 * @author      : oli (oli@oli-HP)
 * @file        : main
 * @created     : Thursday Nov 10, 2022 10:26:07 GMT
 */

#include <iostream>

#include <qgot_public/math-code/state.hpp>

int main()
{
    QGot::State state{3};
    state.print();
    
    return 0;
}

